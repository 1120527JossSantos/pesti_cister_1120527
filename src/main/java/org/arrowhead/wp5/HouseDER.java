/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arrowhead.wp5;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.arrowhead.wp5.core.entities.AbstractDER;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferException;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;
import org.arrowhead.wp5.core.entities.FlexOfferSlice;
import org.arrowhead.wp5.core.impl.FlexOfferAgent;

/**
 *
 * @author ID0087D
 */
public class HouseDER extends AbstractDER{
    
    FlexOfferAgent foa;
    
    public HouseDER(FlexOfferAgent foa) {
        super("myname", "mytype");
        this.foa = foa;
        this.foa.registerDer(this);
    }
    
    public void generateFlexOffer(Date start, Date end) {
         FlexOffer flexOffer = new FlexOffer();
        /*
         * Here we just use static value to populate the slices of the flexoffer
         */
        List<FlexOfferSlice> slices = new ArrayList<FlexOfferSlice>();
        int loadLow[] = new int[] { 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int loadHigh[] = new int[] { 500, 0, 500, 500,0, 0, 500, 0, 500, 0, 500,0 };
        int sliceDuration = 1;

        for (int i = 0; i < loadLow.length; i++) {
            slices.add(new FlexOfferSlice(sliceDuration, 1, loadLow[i], loadHigh[i]));
        }

        flexOffer.setSlices(slices.toArray(flexOffer.getSlices()));
        Date ct = new java.util.Date();
        flexOffer.setCreationTime(ct);

        Calendar cal = Calendar.getInstance();
        cal.setTime(ct);

        cal.add(Calendar.MINUTE, 10);

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back acceptance*/
        flexOffer.setAcceptanceBeforeTime(cal.getTime());

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back schedule*/
        flexOffer.setAssignmentBeforeTime(cal.getTime());
        System.out.println("Set Assig="+cal.getTime());
        
        cal.add(Calendar.MINUTE, 1);
        /*Minimum time at which the consumption pattern can start*/
        flexOffer.setStartAfterTime(cal.getTime());

        cal.add(Calendar.MINUTE, 5);
        /*Maximum time for the consumption pattern to start*/
        flexOffer.setStartBeforeTime(cal.getTime());
        try {
            foa.createFlexOffer(id, flexOffer);
        } catch (FlexOfferException e) {
            System.out.println("Something went wrong!");
        }
    }

    @Override
    public void updateSchedule(FlexOffer fo) {
        FlexOfferSchedule schedule = fo.getFlexOfferSchedule();
        
    }


    public void generateFlexOffer() {
         FlexOffer flexOffer = new FlexOffer();
        /*
         * Here we just use static value to populate the slices of the flexoffer
         */
        List<FlexOfferSlice> slices = new ArrayList<FlexOfferSlice>();
        int loadLow[] = new int[] { 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int loadHigh[] = new int[] { 500, 0, 500, 500,0, 0, 500, 0, 500, 0, 500,0 };
        int sliceDuration = 1;

        for (int i = 0; i < loadLow.length; i++) {
            slices.add(new FlexOfferSlice(sliceDuration, 1, loadLow[i], loadHigh[i]));
        }

        flexOffer.setSlices(slices.toArray(flexOffer.getSlices()));
        Date ct = new java.util.Date();
        flexOffer.setCreationTime(ct);

        Calendar cal = Calendar.getInstance();
        cal.setTime(ct);

        cal.add(Calendar.MINUTE, 10);

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back acceptance*/
        flexOffer.setAcceptanceBeforeTime(cal.getTime());

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back schedule*/
        flexOffer.setAssignmentBeforeTime(cal.getTime());
        System.out.println("Set Assig="+cal.getTime());
        
        cal.add(Calendar.MINUTE, 1);
        /*Minimum time at which the consumption pattern can start*/
        flexOffer.setStartAfterTime(cal.getTime());

        cal.add(Calendar.MINUTE, 5);
        /*Maximum time for the consumption pattern to start*/
        flexOffer.setStartBeforeTime(cal.getTime());
        try {
            foa.createFlexOffer(id, flexOffer);
        } catch (FlexOfferException e) {
            System.out.println("Something went wrong!");
        }
    }

    
    public void generateFlexOfferDemo() {
         FlexOffer flexOffer = new FlexOffer();
        /*
         * Here we just use static value to populate the slices of the flexoffer
         */
        List<FlexOfferSlice> slices = new ArrayList<FlexOfferSlice>();
        int loadLow[] = new int[] { 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int loadHigh[] = new int[] { 500, 0, 500, 500,0, 0, 500, 0, 500, 0, 500,0 };
        int sliceDuration = 1;

        for (int i = 0; i < loadLow.length; i++) {
            slices.add(new FlexOfferSlice(sliceDuration, 1, loadLow[i], loadHigh[i]));
        }

        flexOffer.setSlices(slices.toArray(flexOffer.getSlices()));
        Date ct = new java.util.Date();
        flexOffer.setCreationTime(ct);

        Calendar cal = Calendar.getInstance();
        cal.setTime(ct);

        cal.add(Calendar.MINUTE, 10);

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back acceptance*/
        flexOffer.setAcceptanceBeforeTime(cal.getTime());

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back schedule*/
        flexOffer.setAssignmentBeforeTime(cal.getTime());
        System.out.println("Set Assig="+cal.getTime());
        
        cal.add(Calendar.MINUTE, 1);
        /*Minimum time at which the consumption pattern can start*/
        flexOffer.setStartAfterTime(cal.getTime());

        cal.add(Calendar.MINUTE, 5);
        /*Maximum time for the consumption pattern to start*/
        flexOffer.setStartBeforeTime(cal.getTime());
        try {
            foa.createFlexOffer(id, flexOffer);
        } catch (FlexOfferException e) {
            System.out.println("Something went wrong!");
        }
    }

    @Override
    public int generateFlexOffer(Date start, Date end, FlexOfferSlice[] slices) {
        FlexOffer flexOffer = new FlexOffer();
        
        flexOffer.setSlices(slices);
        

        /*Need to set some time parameter of the flexoffer*/
        Date ct = new java.util.Date();
        flexOffer.setCreationTime(ct);

        Calendar cal = Calendar.getInstance();
        cal.setTime(ct);
        cal.add(Calendar.MINUTE, 10);

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back acceptance*/
        flexOffer.setAcceptanceBeforeTime(cal.getTime());

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back schedule*/
        flexOffer.setAssignmentBeforeTime(cal.getTime());
        System.out.println("Set Assig="+cal.getTime());
        
        cal.add(Calendar.MINUTE, 10);
        /*Minimum time at which the consumption pattern can start*/
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(ct);
        cal2.add(Calendar.DAY_OF_MONTH, 1);
        cal2.set(Calendar.MINUTE, start.getMinutes());
        cal2.set(Calendar.HOUR_OF_DAY, start.getHours());
        flexOffer.setStartAfterTime(cal2.getTime());
       

        
        /*Maximum time for the consumption pattern to start*/
        cal2.set(Calendar.MINUTE, end.getMinutes());
        cal2.set(Calendar.HOUR_OF_DAY, end.getHours());
        flexOffer.setStartBeforeTime(cal2.getTime());
        
        System.out.println(flexOffer);
        
        try {
            return foa.createFlexOffer(id, flexOffer);
        } catch (FlexOfferException e) {
            System.out.println("Something went wrong!");
        }
        return 0;
    }
    
}
