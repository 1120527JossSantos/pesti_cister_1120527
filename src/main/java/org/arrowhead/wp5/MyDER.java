package org.arrowhead.wp5;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.arrowhead.wp5.core.entities.AbstractDER;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferException;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;
import org.arrowhead.wp5.core.entities.FlexOfferSlice;
import org.arrowhead.wp5.core.impl.FlexOfferAgent;

/*This is the part that you need to implement*/
public class MyDER extends AbstractDER {

    private static final long serialVersionUID = -8899409979845372732L;

    FlexOfferAgent foa;
    
    public MyDER(FlexOfferAgent foa) {
        super("myname", "mytype");
        this.foa = foa;
        this.foa.registerDer(this);
    }
    
    /**
     * Example of a method to generate a FlexOffer
     * @param start
     * @param end
     * @return
     */
    @Override
    public void generateFlexOffer(Date start, Date end) {
        FlexOffer flexOffer = new FlexOffer();
        /*
         * Here we just use static value to populate the slices of the flexoffer
         */
        List<FlexOfferSlice> slices = new ArrayList<FlexOfferSlice>();
        int loadLow[] = new int[] { -500, 1700, -1050, 1100, 500, 1400, 400 };
        int loadHigh[] = new int[] { 500, 1732, -500, 1168, 552, 1471, 453 };
        int sliceDuration = 4;

        for (int i = 0; i < loadLow.length; i++) {
            slices.add(new FlexOfferSlice(sliceDuration, 1, loadLow[i], loadHigh[i]));
        }

        flexOffer.setSlices(slices.toArray(flexOffer.getSlices()));
        

        /*Need to set some time parameter of the flexoffer*/
        Date ct = new java.util.Date();
        flexOffer.setCreationTime(ct);

        Calendar cal = Calendar.getInstance();
        cal.setTime(ct);

        cal.add(Calendar.MINUTE, 10);

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back acceptance*/
        flexOffer.setAcceptanceBeforeTime(cal.getTime());

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back schedule*/
        flexOffer.setAssignmentBeforeTime(cal.getTime());
        System.out.println("Set Assig="+cal.getTime());
        
        cal.add(Calendar.MINUTE, 10);
        /*Minimum time at which the consumption pattern can start*/
        flexOffer.setStartAfterTime(cal.getTime());

        cal.add(Calendar.HOUR, 1);
        /*Maximum time for the consumption pattern to start*/
        flexOffer.setStartBeforeTime(cal.getTime());
        
        try {
            foa.createFlexOffer(id, flexOffer);
        } catch (FlexOfferException e) {
            System.out.println("Something went wrong!");
        }
    }

    @Override
    public void updateSchedule(FlexOffer fo) {
        /*Apply the schedule to the object so that it follows 
         *the assigned consumption pattern*/
        FlexOfferSchedule schedule = fo.getFlexOfferSchedule();
    }

    @Override
    public void generateFlexOfferDemo() {
        FlexOffer flexOffer = new FlexOffer();
        /*
         * Here we just use static value to populate the slices of the flexoffer
         */
        List<FlexOfferSlice> slices = new ArrayList<FlexOfferSlice>();
        int loadLow[] = new int[] { 0, 0, 0 , 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int loadHigh[] = new int[] { 500, 0, 500, 500,0, 0, 500, 0, 500, 0, 500,0 };
        int sliceDuration = 1;

        for (int i = 0; i < loadLow.length; i++) {
            slices.add(new FlexOfferSlice(sliceDuration, 1, loadLow[i], loadHigh[i]));
        }

        flexOffer.setSlices(slices.toArray(flexOffer.getSlices()));
        Date ct = new java.util.Date();
        flexOffer.setCreationTime(ct);

        Calendar cal = Calendar.getInstance();
        cal.setTime(ct);

        cal.add(Calendar.MINUTE, 10);

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back acceptance*/
        flexOffer.setAcceptanceBeforeTime(cal.getTime());

        cal.add(Calendar.MINUTE, 1);
        /*Time before aggregator needs to send back schedule*/
        flexOffer.setAssignmentBeforeTime(cal.getTime());
        System.out.println("Set Assig="+cal.getTime());
        
        cal.add(Calendar.MINUTE, 1);
        /*Minimum time at which the consumption pattern can start*/
        flexOffer.setStartAfterTime(cal.getTime());

        cal.add(Calendar.MINUTE, 5);
        /*Maximum time for the consumption pattern to start*/
        flexOffer.setStartBeforeTime(cal.getTime());
        try {
            foa.createFlexOffer(id, flexOffer);
        } catch (FlexOfferException e) {
            System.out.println("Something went wrong!");
        }
    }

    @Override
    public int generateFlexOffer(Date start, Date end, FlexOfferSlice[] slices) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



}
