package org.arrowhead.wp5;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferException;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;
import org.arrowhead.wp5.core.entities.FlexOfferState;
import org.arrowhead.wp5.core.impl.FlexOfferAgent;
import org.arrowhead.wp5.core.interfaces.FlexOfferAgentProviderIf;

/*Set the path where for the resource*/
@Path("/flexoffers")
public class XFlexOfferResource implements FlexOfferAgentProviderIf{

	/*We need the flexoffer agent to interface with flexoffers*/
	FlexOfferAgent flexOfferAgent;
	
	public XFlexOfferResource(FlexOfferAgent flexOfferAgent) {
		this.flexOfferAgent = flexOfferAgent;
	}
	
	@Override
	public FlexOffer getFlexOffer(int flexOfferId) throws FlexOfferException {
		// Not needed for now
		return null;
	}

	@Override
	public FlexOffer[] getFlexOffers() {
		// Not needed for now
		return null;
	}

	@Override
	public FlexOfferState getFlexOfferState(int flexOfferId) {
		// Not needed for now
		return null;
	}

	@Override
	public void setFlexOfferState(int flexOfferId,
			FlexOfferState flexOfferState, String stateReason)
			throws FlexOfferException {
		// Not needed for now
	}

	/**
	 * Used when receiving schedule
	 * @param flexOfferId
	 * @param flexOfferSchedule
	 */
	@Override
	@Path("/{foid}/schedule")
	@POST
	public void createFlexOfferSchedule(@PathParam("foid") int flexOfferId,
			FlexOfferSchedule flexOfferSchedule) throws FlexOfferException {
		this.flexOfferAgent.createFlexOfferSchedule(flexOfferId, flexOfferSchedule);
	}

	@Override
	public FlexOfferSchedule getFlexOfferSchedule(int flexOfferId) {
		// Not needed for now
		return null;
	}

	@Override
	public void setFlexOfferSchedule(int flexOfferId,
			FlexOfferSchedule flexOfferSch) throws FlexOfferException {
		// Not needed for now		
	}

	@Override
	public void deleteFlexOfferSchedule(int flexOfferId)
			throws FlexOfferException {
		// Not needed for now		
	}

}
