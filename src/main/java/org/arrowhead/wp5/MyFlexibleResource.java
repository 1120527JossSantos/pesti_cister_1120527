package org.arrowhead.wp5;

import java.io.IOException;

import org.arrowhead.wp5.com.xmpp.api.HOXTWrapper;
import org.arrowhead.wp5.com.xmpp.api.ResourceManager;
import org.arrowhead.wp5.com.xmpp.clients.flexofferclient.XFlexOfferSubscriberClient;
import org.arrowhead.wp5.core.entities.AbstractDER;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferException;
import org.arrowhead.wp5.core.impl.FlexOfferAgent;
import org.arrowhead.wp5.core.interfaces.FlexOfferUpdateListener;
import org.arrowhead.wp5.core.services.ArrowheadXMPPServiceManager;
import org.arrowhead.wp5.fom.xresources.XFlexOfferResource;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyFlexibleResource implements FlexOfferUpdateListener {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    /** Encapsulated flex-offer agent, the entity managing the flexoffers */
    private FlexOfferAgent agent;
    
    /** The FlexibleDER that needs to generate the FlexOffers **/
    private AbstractDER flexDER;

    /**
     * The id of this flexoffer manager. Used both for the XMPP network and the
     * uniqueness of flex offers
     */
    private String id = "isepagg";
    

    /** The id of the aggregator found by using Arrowhead service discovery **/
    private String aggId = "aggregator-test-tool";

    /** The password used to connect to the XMPP server */
    private String password="arrowhead";
    /** XMPP Server Host */
    private String xmppServer = "delling.dpt.cs.aau.dk";
    /** XMPP Server Port */
    private int xmppPort = 5222;
    /** XMPP Service */
    private String xmppService = "delling";
    /** XMPP resource */
    private String xmppResource = "demo";
    /** The object that encapsulates HTTP over XMPP functionalities */
    private HOXTWrapper hoxtWrapper;
    /**
     * The object that provides the functionalities to send flexoffers to the
     * aggregator
     */
    private XFlexOfferSubscriberClient xfosc;

    /** Interface to use the Arrowhead Service discovery **/
    private ArrowheadXMPPServiceManager foServiceManager;

    static private MyFlexibleResource instance;
    
    public MyFlexibleResource() {
        this.agent = new FlexOfferAgent(id, this);
        this.foServiceManager = new ArrowheadXMPPServiceManager("alpha.jks", "abc1234", "alpha.jks", "abc1234");
        this.flexDER = new HouseDER(this.agent);
        this.doAggregatorSD();
        this.initXmpp();
    }

    public static MyFlexibleResource returnFR () {
        if(instance==null)
            instance= new MyFlexibleResource();
        return instance;
    }

    public FlexOfferAgent getAgent() {
        return agent;
    }

    public AbstractDER getFlexDER() {
        return flexDER;
    }
    
    
    
    
    private void doAggregatorSD() {
        this.foServiceManager.start();

        if (this.foServiceManager.fetchInfo()) {
            aggId = this.foServiceManager.getAggId();
            xmppServer = this.foServiceManager.getHostname();
            xmppPort = this.foServiceManager.getPort();
            xmppResource = this.foServiceManager.getResource();
            logger.info("Aggregator ID set from Arrowhead framework: {}", aggId);
        } else {
            logger.warn("Failed to discover aggregator through Arrowhead framework. Using default config");
        }
    }

    public void initXmpp() {
        /**
         * Need to load the trustStore to connect to the XMPP server Can be done
         * in the code like here or using runtime parameter
         * **/
        System.setProperty("javax.net.ssl.trustStore",
                "resources/clientstore.jks");
        /*Manages REST resources for HTTP over XMPP*/
        ResourceManager resourceManager = new ResourceManager();
        /*Register the interface to the flex offer (implements FlexOfferAgentProviderIf)*/
        resourceManager.registerInstance(new XFlexOfferResource(agent));
        /*Smack connection, set up password, server...*/
        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration
                .builder()
                .setUsernameAndPassword(id, password)
                .setServiceName(xmppService)
                .setHost(xmppServer)
                .setPort(xmppPort)
                //.setKeystorePath("resources/clientstore.jks")
                .setSecurityMode(SecurityMode.required)
                .setResource(xmppResource)
                .setCompressionEnabled(false).build();
        /*A wrapper for Http over XMPP connection*/
        this.hoxtWrapper = new HOXTWrapper(config, resourceManager);
        try {
            /*Initialize the wraper, true means that we will be using 
             * both server and client functionalities.
             */
            this.hoxtWrapper.init(true);
        } catch (XMPPException | SmackException | IOException | InterruptedException e) {
            logger.error("Failed to initialize XMPP", e);
        }
        /*This is the client interface to the aggregator, to send flexoffer for example*/
        this.xfosc = new XFlexOfferSubscriberClient(aggId, hoxtWrapper);
    }

    public void deinitXmpp() {
        /*Clean up the wrapper*/
        this.hoxtWrapper.destroy();
        this.xfosc = null;
    }

    @Override
    public void onFlexOfferScheduleUpdate(FlexOffer fo) {
        /* Forwards the FlexOffer with the Schedule to the FlexibleDER */
        flexDER.updateSchedule(fo);
    }

    /* Callback for the FlexOfferAgent when it creates a flexoffer
     * We send the flexoffer to the aggregator throught the XMPP interface
     * */
    @Override
    public void onFlexOfferCreate(FlexOffer fo) {
        try {
            if (this.xfosc != null) {
                this.xfosc.createFlexOffer(id, fo);
            }
        } catch (FlexOfferException e) {
            // TODO Handle error
            logger.error("Error creating flexoffer.", e);
        }

    }

}
