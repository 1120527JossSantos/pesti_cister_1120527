/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.DAO;
import VPS_Services.VPSController;
import Models.Device;
import Models.Measurement;
import Models.Sensor;
import java.util.ArrayList;
import java.util.Date;
import org.arrowhead.wp5.core.entities.FlexOffer;

/**
 *
 * @author JossSantos
 */
public class DeviceController {
    
    private Device Device;
    private DAO database;
    private VPSController ISA;
    

    public DeviceController(String DeviceID) {
        this.Device=HouseController.getInstance().getDeviceByID(DeviceID);
        database=DAO.getInstance();
        ISA =VPSController.getInstance();
    }
    
    public void addSensorToDevice(String Name){
        Sensor sensor = new Sensor(Device.getID(), Name);
        sensor.setId( VPSController.getInstance().getTag(Device.getID(), Name));
        Device.addNewSensor(sensor);
        database.insertSensor(sensor,Device.getID());
        
    }
    public void addMeasurement(String Measuring, double Value,Date Date){
        Measurement measure = new Measurement(Measuring, Value, Date);
        database.insertMeasurement(measure, Device.getID());
    }
    
    public Sensor getSensorByName(String name){
        ArrayList<Sensor>list = this.Device.getSensors();
        for(Sensor sense: list)
            if(name.equalsIgnoreCase(sense.getName()))
                return sense;
        return null;
    }
    
    public void getMeasurements(int sensorID,long from, long to){
        ArrayList<Measurement> measures = ISA.getMeasurement(getSensorByID(sensorID), from, to);
        measures.stream().forEach((mes) -> {
            addMeasurement(mes.getMeasuring(), mes.getValue(), mes.getDate());
        });
    }

    public void actuate(int command) {
        for(Sensor sens : Device.getSensors()){
            if(sens.getName().equalsIgnoreCase("Actuator")){
                VPSController.getInstance().actuate(sens.getId(),command);
            }
        }
            
    }
    
    public void addFO(FlexOffer fo,String Name){
        
        DAO.getInstance().deactivateFO(this.Device.getID());
        DAO.getInstance().insertFO(fo, this.Device.getID(),Name);
        
    }
    
    public void removeSensor(int sensorID){
        this.Device.getSensors().remove(getSensorByID(sensorID).getId());
        database.deleteSensor(sensorID);
    }

    public Sensor getSensorByID(int sensorID) {
        ArrayList<Sensor>list = this.Device.getSensors();
        for(Sensor sense: list)
            if(sensorID==sense.getId())
                return sense;
        return null;
    }
    
    public ArrayList<Sensor> returnSensors(){
        return Device.getSensors();
    }

    public Object returnPowerMeasurements(long parseLong, long parseLong0) {
        return VPSController.getInstance().getMeasurement(getSensorByName("Active Power"), parseLong, parseLong0);
    }

    public FlexOffer getFO() {
        return DAO.getInstance().returnFOByDeviceID(this.Device.getID());
    }
}
