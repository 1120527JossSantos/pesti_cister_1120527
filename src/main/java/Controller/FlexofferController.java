/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DTO.FlexOfferDTO;
import DTO.ScheduleDTO;
import java.util.ArrayList;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;
import org.arrowhead.wp5.core.entities.FlexOfferSlice;

/**
 *
 * @author JossSantos
 */
public class FlexofferController {

    static FlexofferController instance;

    public FlexofferController() {
    }

    public ArrayList<FlexOfferDTO> convertListToDTOList(ArrayList<FlexOffer> list) {
        ArrayList<FlexOfferDTO> returnList = new ArrayList<>();
        
        for (FlexOffer fo : list) {
        ArrayList<Double> upper = new ArrayList<>();
        ArrayList<Double> lower = new ArrayList<>();
            for (FlexOfferSlice slice : fo.getSlices()) {
                upper.add(slice.getEnergyUpper());
                lower.add(slice.getEnergyLower());
            }

            returnList.add(new FlexOfferDTO(fo.getStartAfterInterval(), fo.getStartBeforeInterval(), upper, lower));
            
        }
        return returnList;
    }

    public static FlexofferController getInstance() {
        if (instance == null) {
            instance = new FlexofferController();
        }
        return instance;
    }

    public FlexOfferDTO convertToDTO(FlexOffer fo) {
        ArrayList<Double> upper = new ArrayList<>();
        ArrayList<Double> lower = new ArrayList<>();
        for (FlexOfferSlice slice : fo.getSlices()) {
            upper.add(slice.getEnergyUpper());
            lower.add(slice.getEnergyLower());
        }

        return new FlexOfferDTO(fo.getStartAfterTime().getTime(), fo.getStartBeforeTime().getTime(), upper, lower);

    }
    public ArrayList<ScheduleDTO> convertListToDTOListForSchedule (ArrayList<FlexOfferSchedule> list) {
        ArrayList<ScheduleDTO> returnList = new ArrayList<>();
        
        list.stream().forEach((fo) -> {
            returnList.add(new ScheduleDTO(fo));
        });
        return returnList;
    }
}
