/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.DAO;
import DTO.Statistics;
import VPS_Services.VPSController;
import Models.Device;
import Models.Room;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.arrowhead.wp5.core.entities.FlexOffer;

/**
 *
 * @author ID0087D
 */
public class HouseController {
    private static ArrayList<Room> Rooms = new ArrayList<>();
    private static ArrayList<Device> Devices = new ArrayList<>();
    static private HouseController instance;
    private static DAO database;
    private static VPSController ISA;
    static private ArrayList<String> sensorNames=new ArrayList<String>(Arrays.asList("Voltage RMS","Frequency","Active Power","Active energy+","Actuator","Power Factor","Power Factor","RSSI","LQI"));
    
    public Room getRoomByID(int ID){
       for(Room rm: Rooms){ 
        if(rm.getID()==ID)
            return rm;
       }
       return null;
    }
    
    public Device getDeviceByID(String ID){
        for(Device dev : Devices){ 
            if(ID.equalsIgnoreCase(dev.getID()))
                return dev;
       }
       return null;
    }

    public HouseController() {
        database = DAO.getInstance();
        ISA = VPSController.getInstance();
    }
    
   public static HouseController getInstance(){
        if(instance==null)
            instance=new HouseController();
        return instance;
    }
    
    public  ArrayList<Device> getDevices(){
        return Devices;
    }
            
    public  ArrayList<Room> getRooms() {
        return Rooms;
    }
    public void addNewRoom(String Name){
        
        Rooms.add(new Room(Name));
        database.insertRoom(Rooms.get(Rooms.size()-1));
    }
    
    public void addNewDevice(String ID,String Name,String room){
        
        Devices.add(new Device(ID, Name, getRoomByName(room)));
        database.insertDevice(Devices.get(Devices.size()-1));
    }
    public void populateHouse(){
        database.populateHouse();
    }
    
    public boolean IsaToken(String user,String pass){
            return ISA.getToken(user, pass);
    }

    

    public ArrayList<Device> getDevicesByRoom(String msg) {
        ArrayList<Device> devices= new ArrayList<>();
        for(Device dev : Devices){ 
            if(msg.equalsIgnoreCase(dev.getRoom().getName()))
                devices.add(dev);
       }
       return devices;
    }
    
    public ArrayList<Device> getDevicesByRoomID(int id) {
        ArrayList<Device> devices= new ArrayList<>();
        for(Device dev : Devices){ 
            if(id==dev.getRoom().getID())
                devices.add(dev);
       }
       return devices;
    }
    
    public ArrayList<Device> getDevicesWithActuator(){
        ArrayList<Device> devices = new ArrayList<>();
        for(Device Dev : Devices)
            if(Dev.hasActuator())
                devices.add(Dev);
        
        return devices;
    }

    public static void setSensorNames(ArrayList<String> sensorNames) {
        HouseController.sensorNames = sensorNames;
    }

    public static ArrayList<String> getSensorNames() {
        return sensorNames;
    }

    private Room getRoomByName(String room) {
        for(Room rm: Rooms){ 
        if(rm.getName().equalsIgnoreCase(room))
            return rm;
       }
       return null;
    }
    
    public Statistics buildStats(){
        Statistics stats = new Statistics();
        stats.setNumberOfFOApplied(database.returnAllFO().size());
        stats.setMoneySaved(stats.getNumberOfFOApplied()*0.05);
        stats.setNumberOfDevicesWithFO(database.retreiveDevicesWithActiveFO().size());
        return stats;
    }
    
}
