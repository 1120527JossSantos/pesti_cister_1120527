/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FH_API;

import DTO.FlexOfferDTO;
import Controller.DeviceController;
import Controller.FlexofferController;
import Controller.HouseController;
import DAO.DAO;
import DTO.ScheduleDTO;
import Models.Device;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;

/**
 *
 * @author JossSantos
 */
@Path("Flexoffer")
public class FlexofferPath {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String allFO() {
        Gson gson = new Gson();
        ArrayList<FlexOffer> devs = DAO.getInstance().returnAllFO();
        ArrayList<FlexOfferDTO> fo = FlexofferController.getInstance().convertListToDTOList(devs);
        String jsonInString = gson.toJson(fo);
        return jsonInString;
    }
    @GET
    @Path("GetAllActive")
    @Produces(MediaType.APPLICATION_JSON)
    public String allActiveFO() {
        Gson gson = new Gson();
        ArrayList<FlexOffer> devs = DAO.getInstance().returnAllActiveFO();
        ArrayList<FlexOfferDTO> fo = FlexofferController.getInstance().convertListToDTOList(devs);
        String jsonInString = gson.toJson(fo);
        return jsonInString;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{param}")
    public String getIt(@PathParam("param") String msg) {
        Gson gson = new Gson();
        DeviceController devCont = new DeviceController(msg);
        FlexOffer devs = devCont.getFO();
        String jsonInString="";
        try{
            jsonInString = gson.toJson(FlexofferController.getInstance().convertToDTO(devs));
       
        }catch(Exception ex){
            System.out.println(ex);
        } 
        return jsonInString;
        
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/Schedule/{param}")
    public String getSchedule(@PathParam("param") String msg) {
        Gson gson = new Gson();
        FlexOfferSchedule devs = DAO.getInstance().returnFOSByDeviceID(msg);
        String jsonInString = gson.toJson(new ScheduleDTO(devs));
        return jsonInString;
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/Day/{param}")
    public String getFlexofferByDay(@PathParam("param") String msg, @QueryParam("Day")String day) {
        Gson gson = new Gson();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateStr=null;
        try {
            dateStr = formatter.parse(day);
        } catch (ParseException ex) {
            Logger.getLogger(FlexofferPath.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<FlexOfferDTO> devs = DAO.getInstance().returnFOByDeviceIDByDay(msg, dateStr);
        
        String jsonInString = gson.toJson(devs);
        return jsonInString;
    }
     @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/Schedule/Day/{param}")
    public String getScheduleByDay(@PathParam("param") String msg, @QueryParam("Day")String day) {
        Gson gson = new Gson();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateStr=null;
        try {
            dateStr = formatter.parse(day);
        } catch (ParseException ex) {
            Logger.getLogger(FlexofferPath.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<ScheduleDTO> devs = DAO.getInstance().returnFOSByDayByDevice(msg, dateStr);
        
        String jsonInString = gson.toJson(devs);
        return jsonInString;
    }
    
    @POST
    @Path("/{param}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postFO(@PathParam("param") String DeviceID, String JSON) {
        Gson gson = new Gson();
        FlexOfferDTO fo = null;
        try{
             fo = gson.fromJson(JSON, FlexOfferDTO.class);
        }catch(Exception ex){
            System.out.println(ex);
        }
        DeviceController devCont = new DeviceController(DeviceID);
        devCont.addFO(fo.toArrowheadFO(),fo.getName());
        return Response.ok("All good").build();
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteFO(@QueryParam("DeviceID") String deviceID) {
        DAO.getInstance().deleteFO(deviceID);
    }
    
}
