/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FH_API;

import Controller.HouseController;
import DAO.DAO;
import DTO.Login_Session;
import com.google.gson.Gson;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author JossSantos
 */
@Path("House")
public class HousePath {

    public HousePath() {
        System.out.println("");
    }
     @Path("Stats")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getStats(){
        Gson gson = new Gson();
        return gson.toJson(HouseController.getInstance().buildStats());
    }
    
    @Path("Room")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteRoom(@QueryParam("RoomID") int roomID){
        DAO.getInstance().deleteRoom(roomID);
    }
    @Path("Room")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void registerNewRoom(String JSON){
        Gson gson = new Gson();
        String room = gson.fromJson(JSON, String.class);
        HouseController.getInstance().addNewRoom(room);
    }
    
    @Path("Room")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllRooms(){
        Gson gson = new Gson();
        return gson.toJson(HouseController.getInstance().getRooms());
    }
    
    @Path("Room/{param}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllDeviceByRoom(@PathParam("param") String msg){
        Gson gson = new Gson();
        return gson.toJson(HouseController.getInstance().getDevicesByRoomID(Integer.parseInt(msg)));
    }
    
   @Path("Login")
   @PUT
   @Consumes(MediaType.APPLICATION_JSON)
   public Response loginAtVPS(String JSON){
       Gson gson = new Gson();
       Login_Session log = gson.fromJson(JSON, Login_Session.class);
       boolean response=HouseController.getInstance().IsaToken(log.getLogin(), log.getPassword());
       if(response)
           return Response.ok().build();
       return Response.serverError().build();
       
      
       
   }
    
    
}