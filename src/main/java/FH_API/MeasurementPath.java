/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FH_API;

import Controller.DeviceController;
import DAO.DAO;
import DTO.MeasurementsDTO;
import com.google.gson.Gson;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author JossSantos
 */
@Path("Measurements")
public class MeasurementPath {
    
  
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteMeasurements(@QueryParam("DeviceID") String deviceID,@QueryParam("Name")String MType){
        DAO.getInstance().deleteMeasurement(deviceID, MType);
    }
    
    @GET
    public String getMeasurementsFromDateAndType(@QueryParam("DeviceID") String deviceID,@QueryParam("from")String from,@QueryParam("to")String to){
        Gson gson = new Gson();
        DeviceController devCont = new DeviceController(deviceID);
        return gson.toJson(devCont.returnPowerMeasurements(Long.parseLong(from),Long.parseLong(to)));
    }
    
    
}
