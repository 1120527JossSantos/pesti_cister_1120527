/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FH_API;

import DTO.StringParameter;
import Controller.DeviceController;
import Controller.HouseController;
import DAO.DAO;
import DTO.ActuationFH;
import DTO.IntegerParameter;
import DTO.NewDeviceDTO;
import DTO.SensorDTO;
import Models.Device;
import Models.Sensor;
import com.google.gson.Gson;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.*;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author JossSantos
 */
@Path("Device")
public class DevicePath {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String allDevices() {
        Gson gson = new Gson();
        ArrayList<Device> devs = HouseController.getInstance().getDevices();
        String jsonInString = gson.toJson(devs);
        return jsonInString;
    }

    @GET
    @Path("GetActuatable")
    @Produces(MediaType.APPLICATION_JSON)
    public String allActuatableDevices() {
        Gson gson = new Gson();
        ArrayList<Device> devs = HouseController.getInstance().getDevicesWithActuator();
        String jsonInString = gson.toJson(devs);
        return jsonInString;
    }

    @GET
    @Path("/{param}")
    public String getDevice(@PathParam("param") String msg) {
        Gson gson = new Gson();
        Device devs = HouseController.getInstance().getDeviceByID(msg);
        String jsonInString = gson.toJson(devs);
        return jsonInString;
    }

    @GET
    @Path("/ByRoom/{param}")
    public String getDeviceByRoom(@PathParam("param") String msg) {
        Gson gson = new Gson();
        ArrayList<Device> devs = HouseController.getInstance().getDevicesByRoom(msg);
        String jsonInString = gson.toJson(devs);
        return jsonInString;
    }

    @POST
    @Path("/Actuate")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response actuate(String deviceIDJSON) {
        Gson gson = new Gson();
        ActuationFH actuation = gson.fromJson(deviceIDJSON, ActuationFH.class);
        DeviceController devc = new DeviceController(actuation.getID());
        devc.actuate(actuation.getCommand());
        return Response.ok("All good").build();
    }

    @DELETE
    @Path("/Sensor")
    @Consumes(MediaType.APPLICATION_JSON)
    public String removeSensor(@QueryParam("DeviceID") String deviceID, @QueryParam("SensorID") int sensorID) {
        DeviceController devCont = new DeviceController(deviceID);
        devCont.removeSensor(sensorID);
        return "All Good";
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String registerDevice(String deviceIDJSON) {
        Gson gson = new Gson();
        NewDeviceDTO device = gson.fromJson(deviceIDJSON, NewDeviceDTO.class);
        HouseController.getInstance().addNewDevice(device.getID(), device.getName(), device.getRoom().getName());
        return "All good";
    }

    @POST
    @Path("/Sensor")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addSensor(String JSON) {
        Gson gson = new Gson();
        SensorDTO sensor = gson.fromJson(JSON, SensorDTO.class);
        DeviceController devCont = new DeviceController(sensor.getDeviceID());
        devCont.addSensorToDevice(sensor.getName());
    }
    
    @GET
    @Path("/Sensor/{param}")
    @Consumes(MediaType.APPLICATION_JSON)
    public String getSensorsByDevice(@PathParam("param") String msg) {
        Gson gson = new Gson();
        DeviceController devCont = new DeviceController(msg);
        return gson.toJson(devCont.returnSensors());
    }
    
    @GET
    @Path("/Sensor/Types")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSensorsTypes() {
        Gson gson = new Gson(); 
        return gson.toJson(HouseController.getSensorNames());
    }
    

}
