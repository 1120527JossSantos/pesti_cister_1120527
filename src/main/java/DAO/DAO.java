/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Controller.HouseController;
import DTO.FlexOfferDTO;
import DTO.ScheduleDTO;
import VPS_Services.VPSController;
import Models.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.arrowhead.wp5.MyFlexibleResource;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;
import org.arrowhead.wp5.core.entities.FlexOfferSlice;
import org.arrowhead.wp5.core.impl.FlexOfferAgent;

/**
 *
 * @author ID0087D
 */
public class DAO {

    String host = "jdbc:derby://localhost:1527/FlexHousing";
    String user = "administrator";
    String pass = "cister";

    static Connection con;
    static DAO instance;

    public DAO() {
        try {
            DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
            con = DriverManager.getConnection(host, user, pass);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }

    }

    public static DAO getInstance() {
        if(instance == null) {
         instance = new DAO();
      }
      return instance;
        
    }

    /*
    List<Employee> findAll();
    List<Employee> findById();
    List<Employee> findByName();
    boolean insertEmployee(Employee employee);
    boolean updateEmployee(Employee employee);
    boolean deleteEmployee(Employee employee);
     */
    public void populateHouse(){
        HouseController house = HouseController.getInstance();
        String statement ="";
        PreparedStatement st = null;
        Boolean isInDB=false;
        try {
            statement="SELECT * FROM ROOM";
            st = con.prepareStatement(statement);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Room temp = new Room(rs.getString(2));
                temp.setID(rs.getInt(1));
                house.getRooms().add(temp);
            }
            
        }catch(SQLException err){
            System.out.println(err.getMessage());
        }
        statement = "Select * FROM Device";
        try {
            st = con.prepareStatement(statement);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                house.getDevices().add(new Device(rs.getString(1), rs.getString(2), house.getRoomByID(rs.getInt(3))));
            }

        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        
    }
    public ArrayList<Device> returnAllDevices() {
        HouseController house = HouseController.getInstance();
        ArrayList<Device> devices = new ArrayList<>();
        String statement = "Select * FROM Device";
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(statement);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                devices.add(new Device(rs.getString(1), rs.getString(2), house.getRoomByID(rs.getInt(3))));
            }

        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        return devices;
    }

    public ArrayList<Device> returnDevicesByRoom(String room) {
        HouseController house = HouseController.getInstance();
        ArrayList<Device> devices = new ArrayList<>();
        String statement ="";
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM DEVICE, ROOM WHERE ROOM.NAME=? and ROOM.ID=DEVICE.ROOMID";
            st = con.prepareStatement(statement);
            st.setString(1,room );
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                devices.add(new Device(rs.getString(1), rs.getString(2), house.getRoomByID(rs.getInt(3))));
            }
            
        }catch(SQLException err){
            System.out.println(err.getMessage());
        }
        return devices;
    }

    public ArrayList<Device> returnDevicesWithActuators() {
        return null;
    }

    public Device returnDeviceByID(String ID) {
        return null;
    }

    public void insertDevice(Device device) {
        String statement ="";
        PreparedStatement st = null;
        Boolean isInDB=false;
        try {
            statement="SELECT * FROM DEVICE";
            st = con.prepareStatement(statement);
             ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString(1).equals(device.getID())){
                    isInDB=true;
                    break;
                }
            }
            
        }catch(SQLException err){
            System.out.println(err.getMessage());
        } 
        if(!isInDB){
        try { 
            statement = "INSERT INTO device (ID,RoomID,devicename) VALUES (?,?,?) ";
            st = con.prepareStatement(statement);

            st.setString(1, device.getID());
            st.setInt(2, device.getRoom().getID());
            st.setString(3, device.getName());
            st.execute();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        
        }else{
       
        }
    }

    public void updateDevice(Device Device) {

    }
    public ArrayList<String> retreiveDevicesWithActiveFO(){
        ArrayList<String> deviceIDs = new ArrayList<>();
        String statement ="";
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM Device d, Flexoffer fo WHERE d.ID=fo.Device AND fo.Active=1";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
            
            ResultSet rs = st.executeQuery();
            rs.next();
            deviceIDs.add(rs.getString(1));
            
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
        
        return deviceIDs;
    }

    public void deleteDevice(Device Delete) {
        String statement ="";
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM Device WHERE ID=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
            st.setString(1,Delete.getID() );
             ResultSet rs = st.executeQuery();
            rs.next();
            rs.deleteRow();
            
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
        
    }

    public ArrayList<Room> returnAllRooms() {
        return null;
    }

    public void insertRoom(Room room) {
        
        String statement ="";
        PreparedStatement st = null;
        Boolean isInDB=false;
        try {
            statement="SELECT * FROM ROOM";
            st = con.prepareStatement(statement);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString(2).equalsIgnoreCase(room.getName())){
                    isInDB=true;
                    break;
                }
            }
            
        }catch(SQLException err){
            System.out.println(err.getMessage());
        } 
        if(!isInDB){
        statement = "INSERT INTO room (name) VALUES (?) ";
        st = null;
        try {
            st = con.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);

            st.setString(1, room.getName());
            st.execute();
            ResultSet id = st.getGeneratedKeys();
            
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
       
        }
        
    }

    public void deleteRoom(int roomID) {
        String statement ="";
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM Room WHERE ID=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
            st.setInt(1,roomID );
             ResultSet rs = st.executeQuery();
            rs.next();
            rs.deleteRow();
            
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }

    }

    public void updateRoom(Room room) {

    }

    public ArrayList<Sensor> returnAllSensors() {
return null;
    }

    public ArrayList<Sensor> returnSensorsByDevice(String deviceID) {
return null;
    }

    public void insertSensor(Sensor sensor, String DeviceID) {
        String statement = "INSERT INTO Sensors (ID,Name,DeviceID) VALUES (?,?,?) ";
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(statement);
            st.setInt(1, sensor.getId());
            st.setString(2, sensor.getName());
            st.setString(3,DeviceID);
            st.execute();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        
    }

    public void deleteSensor(int sensorID) {
        String statement ="";
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM Sensor WHERE ID=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
            st.setInt(1,sensorID );
             ResultSet rs = st.executeQuery();
            rs.next();
            rs.deleteRow();
            
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }

    }

    public void updateSensor(Sensor sensor) {

    }

    public ArrayList<Measurement> returnMeasurementsByDeviceAndByType(String DeviceID, String measureType) {
return null;
    }

    public String insertMeasurement(Measurement measurement, String DeviceID) {
        String statement = "INSERT INTO measurements (Name,Value,Time,DeviceID) VALUES (?,?,?,?) ";
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(statement);
            st.setString(1, measurement.getMeasuring());
            st.setDouble(2, measurement.getValue());
            
            st.setTimestamp(3, new Timestamp(measurement.getDate().getTime()));
            st.setString(4, DeviceID);
            st.execute();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        return null;
    }

    public void deleteMeasurement(String DeviceID, String measureType) {
        String statement ="";
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM Measurements WHERE DEVICEID=? AND NAME=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
            st.setString(1,DeviceID );
            st.setString(2,measureType );
            ResultSet rs = st.executeQuery();
            while(rs.next())
                rs.deleteRow();
           
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }

    }

   public ArrayList<FlexOffer> returnAllFO(){
       ArrayList<FlexOffer> returnFO = new ArrayList<>();
       String statement ="";
        
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM FLEXOFFER";
            st = con.prepareStatement(statement);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
            FlexOffer fo = new FlexOffer();
            Calendar cal = Calendar.getInstance();
            cal.setTime(rs.getDate(2));
            cal.add(Calendar.HOUR, 1);
            Date oneHourBack = cal.getTime();
            int foID = rs.getInt(1);
            fo.setStartAfterTime(oneHourBack);
            cal.setTime(rs.getDate(3));
            cal.add(Calendar.HOUR, 1);
            oneHourBack = cal.getTime();
            fo.setStartBeforeTime(oneHourBack);
            String statement2="SELECT * FROM SLICE WHERE SLICE.FLEXOFFERID=? ORDER BY SEQUENCE ASC";
            st = con.prepareStatement(statement2);
            st.setInt(1,foID);
            ResultSet rs2 = st.executeQuery();
            ArrayList<FlexOfferSlice> slices = new ArrayList<>();
            while (rs2.next()) {
                 slices.add(new FlexOfferSlice(1, 1, rs2.getDouble(3), rs2.getDouble(4)));
             }
            fo.setSlices(slices.toArray(fo.getSlices()));
            returnFO.add(fo);
            }
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
        
       return returnFO;
   }
   public FlexOffer returnFOByDeviceID(String DeviceID) {
       String statement ="";
        FlexOffer fo = new FlexOffer();
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM FLEXOFFER WHERE FLEXOFFER.DEVICE=? AND Active=1";
            st = con.prepareStatement(statement);
            st.setString(1,DeviceID );
            ResultSet rs = st.executeQuery();
            rs.next();
            
            int foID= rs.getInt(1);
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(rs.getTimestamp(2).getTime()));
            cal.add(Calendar.HOUR, 1);
            Date oneHourBack = cal.getTime();
            System.out.println(oneHourBack);
            fo.setStartAfterTime(oneHourBack);
            cal.setTime(new Date(rs.getTimestamp(3).getTime()));
            cal.add(Calendar.HOUR, 1);
            oneHourBack = cal.getTime();
            fo.setStartBeforeTime(oneHourBack);
            statement="SELECT * FROM SLICE WHERE SLICE.FLEXOFFERID=? ORDER BY SEQUENCE ASC";
            st = con.prepareStatement(statement);
            st.setInt(1,foID);
            rs = st.executeQuery();
            ArrayList<FlexOfferSlice> slices = new ArrayList<>();
            while (rs.next()) {
                 slices.add(new FlexOfferSlice(1, 1, rs.getDouble(3), rs.getDouble(4)));
             }
            fo.setSlices(slices.toArray(fo.getSlices()));
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
        
        return fo;
        
    }
    public ArrayList<FlexOfferDTO> returnFOByDeviceIDByDay(String DeviceID, Date date) {
       String statement ="";
       ArrayList<FlexOfferDTO> foList = new  ArrayList<>();
        
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM FLEXOFFER WHERE FLEXOFFER.DEVICE=? AND DATE(Starttime)=?";
                      
            st = con.prepareStatement(statement);
            st.setString(1,DeviceID );
            st.setDate(2,new java.sql.Date(date.getTime()) );
            ResultSet rs = st.executeQuery();
            
            while(rs.next()){
            FlexOfferDTO fo = new FlexOfferDTO();
            int foID= rs.getInt(1);
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(rs.getTimestamp(2).getTime()));
            cal.add(Calendar.HOUR, 0);
            Date oneHourBack = cal.getTime();
            System.out.println(oneHourBack);
            fo.setStartTime(oneHourBack.getTime());
            cal.setTime(new Date(rs.getTimestamp(3).getTime()));
            cal.add(Calendar.HOUR, 0);
            oneHourBack = cal.getTime();
            fo.setEndTime(oneHourBack.getTime());
            fo.setName(rs.getString("Name"));
            statement="SELECT * FROM SLICE WHERE SLICE.FLEXOFFERID=? ORDER BY SEQUENCE ASC";
            st = con.prepareStatement(statement);
            st.setInt(1,foID);
            ResultSet rs2 = st.executeQuery();
            ArrayList<Double> slices = new ArrayList<>();
            ArrayList<Double> slices1 = new ArrayList<>();
            while (rs2.next()) {
                 slices.add(rs2.getDouble(3));
                 slices1.add(rs2.getDouble(4));
             }
            fo.setLowerEnergyValues(slices);
            fo.setUpperEnergyValues(slices1);
            foList.add(fo);
            }
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
        
        
        return foList;
        
    }
    public String insertFO(FlexOffer fo, String deviceid, String name) {
        //falta apagar a FO caso ja exista
        String statement = "INSERT INTO FlexOffer (StartTime,Endtime,device,Active,Name) VALUES (?,?,?,1,?) ";
        int localID =0;
        
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(statement,Statement.RETURN_GENERATED_KEYS);

            st.setTimestamp(1, new Timestamp(fo.getStartAfterTime().getTime()));
            st.setTimestamp(2, new Timestamp(fo.getStartBeforeTime().getTime()));
            st.setString(3, deviceid);
            st.setString(4,name);
            st.execute();
            ResultSet rs = st.getGeneratedKeys();
            rs.next();
            localID = rs.getInt(1);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }

        try {

            int i = 1;
            for (FlexOfferSlice fos : fo.getSlices()) {
                statement = "INSERT INTO Slice (Sequence,LowLoad,HighLoad,FlexofferID) VALUES (?,?,?,?) ";
                st = con.prepareStatement(statement);
                st.setInt(1, i);
                st.setDouble(2, fos.getEnergyLower());
                st.setDouble(3, fos.getEnergyUpper());
                st.setInt(4, localID);
                st.execute();
                i++;
            }
        } catch (SQLException err) {
            System.out.println(err.getMessage());

        }
        return "Sucess";
    }

    public void deleteFO(String deviceID) {
        String statement ="";
        PreparedStatement st = null;
        int foID;
        try {
            statement="SELECT * FROM Flexoffer WHERE Flexoffer.DEVICE=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE,Statement.RETURN_GENERATED_KEYS);
            st.setString(1, deviceID);
             ResultSet rs = st.executeQuery();
            rs.next();
            foID= rs.getInt(1);
            
            statement="SELECT * FROM SLICE WHERE SLICE.FLEXOFFERID=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE,Statement.RETURN_GENERATED_KEYS);
            st.setInt(1,foID);
            ResultSet rs2 = st.executeQuery();
            while (rs2.next()) {
                 rs2.deleteRow();
             }
            rs.deleteRow();
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public FlexOfferSchedule returnFOSByDeviceID(String dev) {
        String statement ="";
        FlexOfferSchedule returnFOS = new FlexOfferSchedule();
        PreparedStatement st = null;
        try {
            statement="SELECT * FROM SCHEDULE WHERE SCHEDULE.DEVICEID=?";
            st = con.prepareStatement(statement);
            st.setString(1,dev);
            ResultSet rs = st.executeQuery();
            rs.next();
            int secID= rs.getInt(1);
            Date date2 = new Date(rs.getDate("Day").getTime()+rs.getTime(2).getTime());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date2);
            cal.add(Calendar.HOUR, +1);
            Date oneHourBack = cal.getTime();
            returnFOS.setStartTime(oneHourBack);
            ArrayList<Double> energy = new ArrayList();
            statement="SELECT * FROM SCHEDULESLICE WHERE SCHEDULESLICE.SCHEDULEID=? ORDER BY SEQUENCE ASC";
            st = con.prepareStatement(statement);
            st.setInt(1,secID);
            rs = st.executeQuery();
            while (rs.next()) {
                 energy.add(rs.getDouble(4));
             }
            double[] returnD = new double[energy.size()];
            for (int i =0;i<returnD.length;i++){
                returnD[i]=energy.get(i);
            }
            returnFOS.setEnergyAmounts(returnD);
        }catch(SQLException err) {
            System.out.println(err.getMessage());
        }
        return returnFOS;
    }
    public ArrayList<ScheduleDTO> returnFOSByDayByDevice(String dev, Date date){
        String statement ="";
        ArrayList<ScheduleDTO> returnFOSList = new ArrayList<>();
        PreparedStatement st = null;
        try {
            
            statement = "SELECT * FROM SCHEDULE WHERE SCHEDULE.DEVICEID=? AND DAY=?";
            st = con.prepareStatement(statement);
            st.setString(1, dev);
            st.setDate(2, new java.sql.Date(date.getTime()));
            System.out.println(new java.sql.Date(date.getTime()));
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ScheduleDTO returnFOS = new ScheduleDTO();
                int secID = rs.getInt(1);
                Date date2 = new Date(rs.getDate("Day").getTime() + rs.getTime(2).getTime());
                returnFOS.setName(rs.getString("Flexoffername"));
                Calendar cal = Calendar.getInstance();
                cal.setTime(date2);
                cal.add(Calendar.HOUR, +1);
                Date oneHourBack = cal.getTime();
                returnFOS.setStart(oneHourBack);
                ArrayList<Double> energy = new ArrayList();
                statement = "SELECT * FROM SCHEDULESLICE WHERE SCHEDULESLICE.SCHEDULEID=? ORDER BY SEQUENCE ASC";
                st = con.prepareStatement(statement);
                st.setInt(1, secID);
                ResultSet rs2 = st.executeQuery();
                while (rs2.next()) {
                    energy.add(rs2.getDouble(4));
                }
                double[] returnD = new double[energy.size()];
                for (int i = 0; i < returnD.length; i++) {
                    returnD[i] = energy.get(i);
                }
                returnFOS.setEnergyAmounts(energy);
                returnFOSList.add(returnFOS);
            }
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }

        return returnFOSList;
    }

    public String insertSchedule(FlexOfferSchedule fo, Device dev) {
        int scheduleID = 0;
        String statement = "INSERT INTO SCHEDULE (StartTime,DeviceID,Day) VALUES (?,?) ";
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);

            st.setTime(1, new Time(fo.getStartTime().getTime()));
            st.setString(2, dev.getID());
            st.setDate(3, new java.sql.Date(fo.getStartTime().getTime()));
            st.execute();
            ResultSet id = st.getGeneratedKeys();
            id.next();
            scheduleID = id.getInt(1);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }

        try {
            int i = 1;
            for (double energy : fo.getEnergyAmounts()) {
                statement = "INSERT INTO SCHEDULESLICE (Sequence,EnergyValue,ScheduleID) VALUES (?,?,?) ";
                st = con.prepareStatement(statement);

                st.setInt(1, i);
                st.setDouble(2, energy);
                st.setInt(3, scheduleID);
                st.execute();
                i++;
            }
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        return "Sucess";
    }

    public void deleteSchedule(FlexOfferSchedule fos, String deviceID) {
        String statement = "";
        PreparedStatement st = null;
        int foID;
        try {
            statement = "SELECT * FROM Schedule WHERE Schedule.DEVICEID=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, deviceID);
            ResultSet rs = st.executeQuery();
            rs.next();
            foID = rs.getInt(1);

            statement = "SELECT * FROM SCHEDULESLICE WHERE SCHEDULESLICE.SCHEDULEID=?";
            st = con.prepareStatement(statement, ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, foID);
            ResultSet rs2 = st.executeQuery();
            while (rs2.next()) {
                rs2.deleteRow();
            }
            rs.deleteRow();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }

    }

    public ArrayList<FlexOffer> returnAllActiveFO() {
        ArrayList<FlexOffer> returnFO = new ArrayList<>();
        String statement = "";

        PreparedStatement st = null;
        try {
            statement = "SELECT * FROM FLEXOFFER WHERE FLEXOFFER.ACTIVE=1";
            st = con.prepareStatement(statement);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                FlexOffer fo = new FlexOffer();

                int foID = rs.getInt(1);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(rs.getTimestamp(2).getTime()));
                cal.add(Calendar.HOUR, 1);
                Date oneHourBack = cal.getTime();
                fo.setStartAfterTime(oneHourBack);
                cal.setTime(new Date(rs.getDate(3).getTime()));
                cal.add(Calendar.HOUR, 1);
                oneHourBack = cal.getTime();
                fo.setStartBeforeTime(oneHourBack);
                statement = "SELECT * FROM SLICE WHERE SLICE.FLEXOFFERID=? ORDER BY SEQUENCE ASC";
                st = con.prepareStatement(statement);
                st.setInt(1, foID);
                ResultSet rs2 = st.executeQuery();
                ArrayList<FlexOfferSlice> slices = new ArrayList<>();
                while (rs2.next()) {
                    slices.add(new FlexOfferSlice(1, 1, rs2.getDouble(3), rs2.getDouble(4)));
                }
                fo.setSlices(slices.toArray(fo.getSlices()));
                returnFO.add(fo);
            }
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }

        return returnFO;
    }

    public void updateFOID(FlexOffer fo, String id) {
        String statement = "";

        PreparedStatement st = null;
        try {
            statement = "UPDATE FLEXOFFER SET WP5ID=?, Active=1 WHERE STARTTIME=? AND DEVICE=?";
            st = con.prepareStatement(statement);
            st.setInt(1, fo.getId());
            st.setTimestamp(2, new Timestamp(fo.getStartAfterTime().getTime()));
            st.setString(3, id);
            st.execute();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public void deactivateFO(String id) {
        String statement = "";

        PreparedStatement st = null;
        try {
            statement = "UPDATE FLEXOFFER SET Active=0 WHERE DEVICE=?";
            st = con.prepareStatement(statement);
            st.setString(1, id);
            st.execute();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public Iterable<FlexOffer> returnActiveFlexOfferByDevice(String deviceID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
