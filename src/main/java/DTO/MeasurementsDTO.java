/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author JossSantos
 */
public class MeasurementsDTO {
    String Name,DeviceID;

    public MeasurementsDTO(String Name, String DeviceID) {
        this.Name = Name;
        this.DeviceID = DeviceID;
    }

    public MeasurementsDTO() {
    }

    public String getName() {
        return Name;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setDeviceID(String DeviceID) {
        this.DeviceID = DeviceID;
    }
    
    
    
}
