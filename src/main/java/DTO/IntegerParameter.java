/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author JossSantos
 */
public class IntegerParameter {
    int ID;

    public IntegerParameter(int ID) {
        this.ID = ID;
    }

    public IntegerParameter() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    
}
