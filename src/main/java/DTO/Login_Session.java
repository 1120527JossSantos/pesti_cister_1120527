/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author ID0087D
 */
public class Login_Session {

    String  Login,Password;

    public Login_Session(String Login, String Password) {
        this.Login = Login;
        this.Password = Password;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }

    public Login_Session() {
    }

  
    public void setlogin(String Login) {
        this.Login = Login;
    }


    public void setpassword(String Password) {
        this.Password = Password;
    }

    @Override
    public String toString() {
        return "Login_Session{" + "Login=" + Login + ", Password=" + Password + '}';
    }


}
