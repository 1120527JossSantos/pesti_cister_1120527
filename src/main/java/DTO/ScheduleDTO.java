/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.Date;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;

/**
 *
 * @author JossSantos
 */
public class ScheduleDTO {
    String name;
    Date Start;
    ArrayList<Double> energyAmounts = new ArrayList<>();

    public ScheduleDTO(FlexOfferSchedule fos) {
        this.Start = fos.getStartTime();
        for(double energy : fos.getEnergyAmounts())
            energyAmounts.add(energy);
    }

    public ScheduleDTO() {
    }

    public Date getStart() {
        return Start;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Double> getEnergyAmounts() {
        return energyAmounts;
    }

    public void setStart(Date Start) {
        this.Start = Start;
    }

    public void setEnergyAmounts(ArrayList<Double> energyAmounts) {
        this.energyAmounts = energyAmounts;
    }
    
    
    
    
}
