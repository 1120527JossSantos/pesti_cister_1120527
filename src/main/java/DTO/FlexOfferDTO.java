/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.Date;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferSlice;

/**
 *
 * @author JossSantos
 */
public class FlexOfferDTO {
    
    String name;
    long startTime,endTime;
    ArrayList<Double> upperEnergyValues ;
    ArrayList<Double> lowerEnergyValues ;

    public FlexOfferDTO(long startTime, long endTime, ArrayList<Double> upperEnergyValues, ArrayList<Double> lowerEnergyValues) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.upperEnergyValues = upperEnergyValues;
        this.lowerEnergyValues = lowerEnergyValues;
    }

    public FlexOfferDTO() {
    }

    public long getStartTime() {
        return startTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getEndTime() {
        return endTime;
    }

    public ArrayList<Double> getUpperEnergyValues() {
        return upperEnergyValues;
    }

    public ArrayList<Double> getLowerEnergyValues() {
        return lowerEnergyValues;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public void setUpperEnergyValues(ArrayList<Double> upperEnergyValues) {
        this.upperEnergyValues = upperEnergyValues;
    }

    public void setLowerEnergyValues(ArrayList<Double> lowerEnergyValues) {
        this.lowerEnergyValues = lowerEnergyValues;
    }

   
    
    public FlexOffer toArrowheadFO() {
        FlexOffer fo = new FlexOffer();
        fo.setStartAfterTime(new Date(startTime));
        System.out.println(fo.getStartAfterTime());
        fo.setStartBeforeTime(new Date (endTime));
        System.out.println(fo.getStartBeforeTime());
        FlexOfferSlice[] foSlice= new FlexOfferSlice[upperEnergyValues.size()];
        for(int i=0;i<upperEnergyValues.size();i++){
            foSlice[i] = new FlexOfferSlice(1, 1, lowerEnergyValues.get(i), upperEnergyValues.get(i));
           
        }
        fo.setSlices(foSlice);
        return fo;
    }
    
}
