/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author ID0087D
 */
public class ActuationVPS {

    String Command;
    ArrayList<Integer> TagIds = new ArrayList<>();

    public ActuationVPS(String Command, int TagIds) {
        this.Command = Command;
        this.TagIds.add(TagIds);
    }

    public String getCommand() {
        return Command;
    }

    public ArrayList<Integer> getTagIds() {
        return TagIds;
    }

    @JsonProperty("Command")
    public void setCommand(String Command) {
        this.Command = Command;
    }

    @JsonProperty("TagIds")
    public void setTagIds(ArrayList<Integer> TagIds) {
        this.TagIds = TagIds;
    }

}
