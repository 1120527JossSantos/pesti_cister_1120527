/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author JossSantos
 */
public class Statistics {
    int numberOfDevicesWithFO,numberOfFOApplied;
    double        moneySaved;

    public Statistics() {
    }

    public void setNumberOfDevicesWithFO(int numberOfDevicesWithFO) {
        this.numberOfDevicesWithFO = numberOfDevicesWithFO;
    }

    public void setNumberOfFOApplied(int numberOfFOApplied) {
        this.numberOfFOApplied = numberOfFOApplied;
    }

    public void setMoneySaved(double moneySaved) {
        this.moneySaved = moneySaved;
    }

    public int getNumberOfDevicesWithFO() {
        return numberOfDevicesWithFO;
    }

    public int getNumberOfFOApplied() {
        return numberOfFOApplied;
    }

    public double getMoneySaved() {
        return moneySaved;
    }
    
    
}
