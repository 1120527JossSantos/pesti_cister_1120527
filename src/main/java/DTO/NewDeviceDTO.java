/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import Models.Room;

/**
 *
 * @author JossSantos
 */
public class NewDeviceDTO {
    String ID,Name;
    Room room;

    public NewDeviceDTO(String ID, String Name, Room room) {
        this.ID = ID;
        this.Name = Name;
        this.room = room;
    }

    public NewDeviceDTO() {
    }

    public String getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public Room getRoom() {
        return room;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    
    
    
}
