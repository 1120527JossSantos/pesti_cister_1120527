/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author JossSantos
 */
public class ActuationFH {
    String ID;
    int command;

    public ActuationFH(String ID, int command) {
        this.ID = ID;
        this.command = command;
    }

    public ActuationFH() {
    }

    public String getID() {
        return ID;
    }

    public int getCommand() {
        return command;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setCommand(int command) {
        this.command = command;
    }
    
}
