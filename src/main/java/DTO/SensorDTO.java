/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author JossSantos
 */
public class SensorDTO {
    int sensorID;
    String deviceID,Name;

    public SensorDTO(int sensorID, String deviceID, String Name) {
        this.sensorID = sensorID;
        this.deviceID = deviceID;
        this.Name=Name;
    }

    public SensorDTO() {
    }

    public int getSensorID() {
        return sensorID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setSensorID(int sensorID) {
        this.sensorID = sensorID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
}
