/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VPS_Services;

import Controller.DeviceController;
import Controller.HouseController;
import Execution.Main;
import DTO.ActuationVPS;
import DTO.Login_Session;
import Models.Measurement;
import Models.Sensor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Date;
import org.apache.http.HttpEntity;
import org.xml.sax.SAXException;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import java.util.Calendar;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author JossSantos
 */
public class VPSController {

    static String Token;
    public static final int TURN_ON=0,TURN_OFF=1,TOGGLE=3;
    static CloseableHttpClient httpClient=HttpClients.custom().build();;
    public static final boolean logged=true,NotLogged=false;
    static VPSController instance = null;

    public VPSController() {
        
      
    }
    public static VPSController getInstance(){
        if(instance == null) {
         instance = new VPSController();
      }
      return instance;
    }

    public boolean getToken(String email,String password) {
        httpClient = HttpClients.custom().build();
        HttpPost postRequest = new HttpPost("http://innov.isaenergy.pt:6600/api/1.4/sessions");
        Login_Session test = new Login_Session(email, password);
        ObjectMapper mapper = new ObjectMapper();
        Gson gson = new Gson();
        String jsonInString = null;
        jsonInString = gson.toJson(test);
        StringEntity input = null;
        try {
            input = new StringEntity(jsonInString);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        input.setContentType("application/json");
        postRequest.setEntity(input);
        HttpResponse response = null;
        try {
            response = httpClient.execute(postRequest);
        } catch (IOException ex) {
            Logger.getLogger(VPSController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(response.getStatusLine().getStatusCode()==401){
            return NotLogged;
        }
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException ex) {

        } catch (UnsupportedOperationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        StringBuffer result = new StringBuffer();
        String line = "";
        try {
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        String resultString = result.toString();
        String[] campos = resultString.split("\"");
        Token = campos[9].replace("\\", "");
        return logged;
    }

    public void actuate(int actuationTag,int operation) {
        httpClient = HttpClients.custom().build();
        HttpPost postRequest = new HttpPost("http://innov.isaenergy.pt:6600/api/1.4/actuations");
        postRequest.addHeader(HttpHeaders.AUTHORIZATION, "ISA " + Token);
        ActuationVPS test = new ActuationVPS(operation+"", actuationTag);
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(test);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        StringEntity input = null;
        try {
            input = new StringEntity(jsonInString);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        input.setContentType("application/json");

        postRequest.setEntity(input);

        HttpResponse response = null;
        try {
            response = httpClient.execute(postRequest);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public ArrayList<Measurement> getMeasurement(Sensor Tag,long from, long to){
       HttpGet getRequest = new HttpGet("http://innov.isaenergy.pt:6600/api/1.4/consumptions/instant?from="+from+"&to="+to+"&tags="+Tag.getId());
//        HttpGet getRequest = new HttpGet("http://innov.isaenergy.pt:6600/api/1.4/consumptions/instant?from="+from+"&to="+to+"&tags=9454");
        getRequest.addHeader(HttpHeaders.AUTHORIZATION, "ISA " + Token);
        HttpResponse response = null;
        try {
            response = httpClient.execute(getRequest);

//HttpEntity entity = response.getEntity();
//String responseString = EntityUtils.toString(entity, "UTF-8");
//System.out.println(responseString);

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        Document doc = null;
        HttpEntity entity = response.getEntity();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(response.getEntity().getContent());
        
        } catch (ParserConfigurationException | IllegalStateException | SAXException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(VPSController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Node list = doc.getFirstChild();
        String text="";
        ArrayList<Measurement> measures = new ArrayList<>();
        NodeList measurements = list.getChildNodes();
        for (int i = 0; i < measurements.getLength(); i++) {
            
            Date date2 =null;
            Date date=null;
            double temp2 = 0.0;
            NodeList nodes = (NodeList) measurements.item(i).getChildNodes();
            for (int temp = 0; temp < nodes.getLength(); temp++) {
                Node node = nodes.item(temp);
                Element eElement = (Element) node;
                        
                
                    if ("Date".equals(eElement.getNodeName())) {
                        date2 = new Date(Long.parseLong(eElement.getTextContent()));
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date2);
                        cal.add(Calendar.HOUR, -1);
                        date = cal.getTime();
                        
                      
                    }    
                    if ("Read".equals(eElement.getNodeName())){
                        
                        temp2 = Double.parseDouble(eElement.getTextContent());
                       
                        
                    }
                  
                
                  
            }
            measures.add(new Measurement(Tag.getName(), temp2, date));  
        }
        
        return measures;
        
    }

    public int getTag(String deviceID,String TagName)  {
        int flag = 0;
        int tag = 0;
        int device=0;
        int temptag=0;
        HttpGet postRequest = new HttpGet("http://innov.isaenergy.pt:6600/api/1.4/devices");
        postRequest.addHeader(HttpHeaders.AUTHORIZATION, "ISA " + Token);
        HttpResponse response = null;
        try {
            response = httpClient.execute(postRequest);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        Document doc = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(response.getEntity().getContent());
        } catch (ParserConfigurationException | IllegalStateException | SAXException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(VPSController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Node list = doc.getElementsByTagName("List").item(0);
        NodeList devices = list.getChildNodes();
        for (int i = 0; i < devices.getLength(); i++) {
            NodeList nodes = (NodeList) devices.item(i).getChildNodes();
            for (int temp = 0; temp < nodes.getLength(); temp++) {
                Node node = nodes.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    if ("a:Address".equals(eElement.getNodeName())) {
                        if (deviceID.equals(eElement.getTextContent())) {
                            flag = 1;
                        }
                    }
                    if ("a:Id".equals(eElement.getNodeName()) && flag == 1) {

                        device = Integer.parseInt(eElement.getTextContent());
                    }
                }

            }
            flag = 0;
        }
        postRequest = new HttpGet("http://innov.isaenergy.pt:6600/api/1.4/tags");
        postRequest.addHeader(HttpHeaders.AUTHORIZATION, "ISA " + Token);
        response = null;
        try {
            response = httpClient.execute(postRequest);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        doc = null;
       
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(response.getEntity().getContent());
        } catch (ParserConfigurationException | IllegalStateException | SAXException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(VPSController.class.getName()).log(Level.SEVERE, null, ex);
        }
        list = doc.getElementsByTagName("List").item(0);
        NodeList tags = list.getChildNodes();
        
        for (int i = 0; i < tags.getLength(); i++) {
            NodeList nodes = (NodeList) tags.item(i).getChildNodes();
            for (int temp = 0; temp < nodes.getLength(); temp++) {
                Node node = nodes.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    if ("a:DeviceId".equals(eElement.getNodeName())) {
                        if (device==Integer.parseInt(eElement.getTextContent())) {
                            flag = 1;
                        }
                    }
                     if ("a:Id".equals(eElement.getNodeName())) {
                         temptag = Integer.parseInt(eElement.getTextContent());
                         flag++;
                     }
                    if ("a:Name".equals(eElement.getNodeName()) && flag == 2) {
                        if (TagName.equals(eElement.getTextContent()))
                        tag=temptag;
                    }
                }
               
            } flag=0;
        }
        return tag;
    }
    
}