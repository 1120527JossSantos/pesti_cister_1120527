/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Execution;

import Controller.DeviceController;
import DAO.DAO;
import Models.Device;
import Controller.HouseController;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.arrowhead.wp5.core.entities.FlexOffer;

/**
 *
 * @author JossSantos
 */
class FlexofferTimer extends TimerTask {

    public FlexofferTimer() {
    }

    @Override
    public void run() {
        ArrayList<String> deviceIDs = DAO.getInstance().retreiveDevicesWithActiveFO();
        ExecutorService executor = Executors.newFixedThreadPool(deviceIDs.size());
        deviceIDs.stream().forEach((deviceID) -> {
            DeviceController devCont = new DeviceController(deviceID);
            for(FlexOffer fo : DAO.getInstance().returnActiveFlexOfferByDevice(deviceID)){
                Runnable worker = new ExecuteFOEmission(devCont.getFO(),HouseController.getInstance().getDeviceByID(deviceID));
                executor.execute(worker);
            }
        });
    }
    
}
