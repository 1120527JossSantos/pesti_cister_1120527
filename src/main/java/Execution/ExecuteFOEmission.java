/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Execution;

import Controller.HouseController;
import Models.Device;
import static java.lang.Thread.sleep;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.arrowhead.wp5.MyFlexibleResource;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;
import org.arrowhead.wp5.core.entities.FlexOfferState;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import org.arrowhead.wp5.core.entities.FlexOffer;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;
import static java.lang.Thread.sleep;

/**
 *
 * @author JossSantos
 */
 public class ExecuteFOEmission implements Runnable {

    FlexOffer fo;
    Device dev;
    ExecuteFOEmission(FlexOffer fo,Device Dev) {
        this.fo = fo;
        this.dev= Dev;
    }
    //Date start, Date end, ArrayList<FlexOfferSlice> slices
    @Override
    public void run() {
        System.out.println(fo);
        MyFlexibleResource test = MyFlexibleResource.returnFR();
        fo.setId(test.getFlexDER().generateFlexOffer(fo.getStartAfterTime(),fo.getStartBeforeTime(),fo.getSlices()));
        while (test.getAgent().getFlexOfferState(1) == FlexOfferState.Initial) {
            try {
                sleep(1000);
                System.out.println("Waiting");
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        DAO.DAO.getInstance().updateFOID(fo,dev.getID());
        DAO.DAO.getInstance().insertSchedule(test.getAgent().getFlexOfferSchedule(fo.getId()), dev);
        FlexOfferSchedule fos = DAO.DAO.getInstance().returnFOSByDeviceID(dev.getID());
        System.out.println(fos.specialToString());

    }

}
