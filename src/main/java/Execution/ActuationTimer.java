package Execution;

import DAO.DAO;
import Models.Device;
import Controller.HouseController;
import DTO.ScheduleDTO;
import Models.NextDaySchedule;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;

/**
 *
 * @author JossSantos
 */
class ActuationTimer extends TimerTask {

    public ActuationTimer() {
    }

    @Override
    public void run() {
        HouseController house= HouseController.getInstance();
        DAO database = DAO.getInstance();
        ArrayList<Device> deviceList = house.getDevices();
        ExecutorService executor = Executors.newFixedThreadPool(deviceList.size());
        for(Device dev : deviceList){
            for(ScheduleDTO fo : database.returnFOSByDayByDevice(dev.getID(), new Date())){
            Runnable worker = new ExecuteActuations(new NextDaySchedule(fo), 15*60, dev);
            executor.execute(worker);
            }
        }
    }
    
}

    
