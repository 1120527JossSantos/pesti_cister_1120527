/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Execution;

import Controller.DeviceController;
import DAO.DAO;
import Controller.HouseController;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Calendar;
import java.net.URI;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author ID0087D
 */
public class Main {

    
    public static final String BASE_URI = "http://192.168.60.44:8081/FlexHousing";

    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.example.rest package
        final ResourceConfig rc = new ResourceConfig();
        rc.packages("FH_API");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * @param args the command line arguments
     * new Room("Sala");
     * new Device("3ZU-VGC-N3J-NWK-9P", "test", sala);
     */
    public static void main(String[] args)  {
        HouseController house = HouseController.getInstance();
        house.populateHouse();
        house.IsaToken("1120527@isep.ipp.pt", "cister");
        
        DeviceController DevCont = new DeviceController("3ZU-VGC-N3J-NWK-9P");
        DevCont.addSensorToDevice("Active Power");
        DevCont.addSensorToDevice("Actuator");
//        DevCont.getMeasurements(DevCont.getSensorByName("Actuator"), 1469441590000l,1469455990000l);
//        MyFlexibleResource test = MyFlexibleResource.returnFR();
//       test.getFlexDER().generateFlexOffer(null, null);
//        DAO.getInstance().updateFOID(test.getAgent().getFlexOffer(1),"3ZU-VGC-N3J-NWK-9P");
//        test.getFlexDER().generateFlexOffer(null, null);
//        System.out.println(database.returnDevicesByRoom("Sala"));
//        System.out.println(database.returnFOByDeviceID("3ZU-VGC-N3J-NWK-9P"));
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        try {
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        server.stop();
    }
    
    public static void startFOEmissions(){
        TimerTask timerTask = new FlexofferTimer();
        Timer timer = new Timer();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();
        timer.scheduleAtFixedRate(timerTask, time, 24 * 60 * 60 * 1000);
    }
    
    public static void startActuations(){
        TimerTask timerTask = new ActuationTimer();
        Timer timer = new Timer();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();
        timer.scheduleAtFixedRate(timerTask, time, 24 * 60 * 60 * 1000);
    }
}
//MyFlexibleResource test = MyFlexibleResource.returnFR();
//        test.getFlexDER().generateFlexOffer(null, null);
//        new Thread(new Runnable() {
//
//            public void run() {
//                System.out.println(test.getAgent().getFlexOfferState(1) + " " + new Date(System.currentTimeMillis()));
//                while (test.getAgent().getFlexOfferState(1) == FlexOfferState.Initial) {
//                    System.out.println("waiting");
//                    try {
//                        sleep(1000);
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                System.out.println(test.getAgent().getFlexOfferState(1) + " " + new Date(System.currentTimeMillis()));
//                System.out.println(test.getAgent().getFlexOfferSchedule(1));
//                database.insertSchedule(test.getAgent().getFlexOfferSchedule(1), (house.getDeviceByID("3ZU-VGC-N3J-NWK-9P")));
//                FlexOfferSchedule fos = database.returnFOSByDeviceID((house.getDeviceByID("3ZU-VGC-N3J-NWK-9P")));
//                System.out.println(fos);
//            }
//        }).start();