/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Execution;

import Controller.DeviceController;
import VPS_Services.VPSController;
import Models.Device;
import Models.Schedule;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.lang.Thread.sleep;

/**
 *
 * @author JossSantos
 */
public class ExecuteActuations implements Runnable{
        Schedule fos;
        int currentState=0;
        int interval;
        DeviceController dev;
        public ExecuteActuations(Schedule fos,int interval,Device dev) {
            this.fos=fos;
            this.interval = interval;
            this.dev=new DeviceController(dev.getID());
        }
        
        @Override
        public void run() {
            for(int comm : fos.getCommutations()){
                if(comm!= currentState){
                    dev.actuate(VPSController.TOGGLE);
                    currentState=comm;
                }
                try {
                    sleep(interval*1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }

