/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;

/**
 *
 * @author JossSantos
 */
public class InfraDaySchedule extends Schedule{
    ArrayList<Integer> comutations=new ArrayList<>();
    
    
    public InfraDaySchedule(FlexOfferSchedule fos) {
        
        Date data = fos.getStartTime();
        int scheduleStarthour =data.getHours()*4+(data.getMinutes()/15);
        Date ct = new java.util.Date();
        int currentHour= ct.getHours()*4+(ct.getMinutes()/15);
        double[] energy = fos.getEnergyAmounts();
        for(int i = currentHour;i<scheduleStarthour+energy.length;i++){
            if(i<scheduleStarthour){
                comutations.add(0);
            }else{
                System.out.println(energy[i-scheduleStarthour]);
                if(energy[i-scheduleStarthour]<=0){
                    comutations.add(0);
                }else{
                    comutations.add(1);
                }
            }
        }
        System.out.println(comutations);
    }

    public int[] getCommutations() {
        return  comutations.stream().mapToInt(i -> i).toArray();
    }
}
