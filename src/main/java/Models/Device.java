/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import VPS_Services.VPSController;
import java.util.ArrayList;
import java.util.Date;
import org.arrowhead.wp5.core.entities.FlexOffer;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;

/**
 *
 * @author ID0087D
 */
public class Device {
    String ID;
 
    Room room;
    String Name;
    FlexOfferSchedule schedule;
    ArrayList<Sensor> sensors= new ArrayList<>();
    

    public Device(String ID,String Name,Room room) {
        this.ID = ID;
        this.room= room;
        this.Name=Name;
    }

   

//    public void setFo(FlexOffer fo) {
//        this.fo = fo;
//    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getName() {
        return Name;
    }

    public void setSchedule(FlexOfferSchedule schedule) {
        this.schedule = schedule;
    }

    public String getID() {
        return ID;
    }

//    public FlexOffer getFo() {
//        return fo;
//    }

    public Room getRoom() {
        return room;
    }

    public FlexOfferSchedule getSchedule() {
        return schedule;
    }

    public ArrayList<Sensor> getSensors() {
        return sensors;
    }

    @Override
    public String toString() {
        return "Device{" + "ID=" + ID + ", room=" + room + ", schedule=" + schedule + ", sensors=" + sensors + '}';
    }
    public void addNewSensor(Sensor sensor){
      sensors.add(sensor);
      
        
    }
    
    
   

    public boolean hasActuator() {
        for(Sensor sense : sensors){
            if(sense.Name.equalsIgnoreCase("Actuator"))
                return true;
        }
        return false;
    }
    
}
