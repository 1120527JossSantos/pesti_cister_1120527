/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author JossSantos
 */
public class Measurement {
    String Measuring;
    double Value;
    Date Date;
    

    public Measurement(String Measuring, double Value, Date Date) {
        this.Measuring = Measuring;
        this.Value = Value;
        this.Date = Date;
        
    }

    public String getMeasuring() {
        return Measuring;
    }

    public double getValue() {
        return Value;
    }

    public Date getDate() {
        return Date;
    }

    

    public void setMeasuring(String Measuring) {
        this.Measuring = Measuring;
    }

    public void setValue(double Value) {
        this.Value = Value;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

 

    @Override
    public String toString() {
        return "At "+Date+", we at a value of "+Value+" for the measurement of "+Measuring+"\n";
    }
    
    
}
