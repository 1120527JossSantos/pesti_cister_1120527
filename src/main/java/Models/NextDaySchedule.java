/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import DTO.ScheduleDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.arrowhead.wp5.core.entities.FlexOfferSchedule;

/**
 *
 * @author ID0087D
 */
public class NextDaySchedule extends Schedule
{
    ArrayList<Integer> comutations=new ArrayList<>();
    int[] commutations= new int[96];
    
    public NextDaySchedule(FlexOfferSchedule fos) {
        Arrays.fill(commutations, 0);
        Date data = fos.getStartTime();
        int hour =data.getHours()*4+(data.getMinutes()/15);
        double[] energy = fos.getEnergyAmounts();
        for(double value : energy){
            if(value>0){
                commutations[hour]=1;
            }else{
                commutations[hour]=0;
            }
        hour++;
        }
    }

    public NextDaySchedule(ScheduleDTO fo) {
        Arrays.fill(commutations, 0);
        Date data = fo.getStart();
        int hour =data.getHours()*4+(data.getMinutes()/15);
        for(double value : fo.getEnergyAmounts()){
            if(value>0){
                commutations[hour]=1;
            }else{
                commutations[hour]=0;
            }
        hour++;
        }
    }

    @Override
    public int[] getCommutations() {
        return commutations;
    }

    
   

    @Override
    public String toString() {
        String ret="";
        for(int i=0;i<comutations.size();i++)
            ret = ret+" , "+comutations.get(i);
        return ret;
    }
    
    
    
}
