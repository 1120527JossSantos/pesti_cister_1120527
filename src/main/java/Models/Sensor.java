/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import VPS_Services.VPSController;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ID0087D
 */
public class Sensor {

    int id;
    String Name;

    public Sensor(String DeviceID, String Name) {
        VPSController isa = VPSController.getInstance();

        int tag = 0;

        

        this.id = tag;
        this.Name = Name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return "Sensor{" + "id=" + id + ", Name=" + Name + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sensor other = (Sensor) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

}
